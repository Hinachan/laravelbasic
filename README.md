# LaravelBasic

Laravelの基本動作を実装。

## 利用方法

`git clone https://gitlab.com/Hinachan/laravelbasic.git`

`composer install`

`cp .env.example .env`

`php artisan key:generate`

.envを環境に合わせてください。

## 注意事項

- データベースは自身で準備してください。
- 動作内容につきましては一切の責任を負えません。（あくまでサンプルです）

## 動作確認

https://laravelbasic.tansan.xyz

上記URLにてmasterが適用されています。
