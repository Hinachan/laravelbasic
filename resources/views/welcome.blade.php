<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Laravel Basic</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/mdb.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div style="height: 100vh">
            <div class="flex-center flex-column">
                <h1 class="animated fadeIn mb-2">Laravel Basic</h1>
                <h5 class="animated fadeIn mb-1">Laravelの基本動作を実装します。</h5>
                <p class="animated fadeIn text-muted">Hinachan</p>
                <a href="https://gitlab.com/Hinachan/laravelbasic">https://gitlab.com/Hinachan/laravelbasic</a>
            </div>
        </div>
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="js/popper.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/mdb.min.js"></script>
    </body>
</html>
